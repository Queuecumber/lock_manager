from .const import ENTITY_ID_FORMAT
import logging
from homeassistant.helpers.restore_state import RestoreEntity

ATTR_CODE = 'code'
ZWAVE_MIN_CODE_LENGTH = 4
ZWAVE_MAX_CODE_LENGTH = 8

_LOGGER = logging.getLogger(__name__)


class LockUser(RestoreEntity):
    def __init__(self, object_id: str, name: str) -> None:
        self.entity_id = ENTITY_ID_FORMAT.format(object_id)
        self._name = name
        self._code = None

    @property
    def should_poll(self) -> bool:
        return False

    @property
    def name(self) -> str:
        return self._name

    @property
    def state(self) -> str:
        return self._code

    async def async_added_to_hass(self) -> None:
        await super().async_added_to_hass()
        if self._code is not None:
            return

        state = await self.async_get_last_state()
        code = state and state.state

        if code is not None and ZWAVE_MIN_CODE_LENGTH <= len(code) <= ZWAVE_MAX_CODE_LENGTH:
            self._code = code

    async def async_set_value(self, value: str) -> None:
        if ZWAVE_MIN_CODE_LENGTH <= len(value) <= ZWAVE_MAX_CODE_LENGTH:
            _LOGGER.warning("Invalid security code length (length range %s - %s)",
                            ZWAVE_MIN_CODE_LENGTH, ZWAVE_MAX_CODE_LENGTH)
            return

        self._code = value
        await self.async_update_ha_state()
