from .const import ENTITY_ID_FORMAT
import logging
from homeassistant.core import HomeAssistant
from homeassistant.helpers.restore_state import RestoreEntity


_LOGGER = logging.getLogger(__name__)

LOCK_NAME_FORMAT = '{} Manager'


class LockManager(RestoreEntity):
    def __init__(self, hass: HomeAssistant,  eid: str) -> None:
        self.entity_id = ENTITY_ID_FORMAT.format(eid.split('.')[1])
        self.managed_entity = eid

        lock_state = hass.states.get(self.managed_entity)
        print(lock_state)

        self._name = 'Manager'
        self._users = []
        self._state = None

    @property
    def should_poll(self):
        return False

    @property
    def name(self):
        return self._name

    @property
    def state(self):
        """Return the state of the component."""
        return self._state

    @property
    def state_attributes(self):
        """Return the state attributes."""
        return {

        }

    async def async_added_to_hass(self):
        await super().async_added_to_hass()

    async def async_set_value(self, value):
        """Select new value."""
        await self.async_update_ha_state()
