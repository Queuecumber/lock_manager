"""Advanced management for ZWave locks"""
import voluptuous as vol
import logging
import homeassistant.helpers.config_validation as cv
from .const import DOMAIN
from .lock_manager import LockManager
from .lock_user import LockUser
from homeassistant.helpers.entity_component import EntityComponent
from homeassistant.core import HomeAssistant


_LOGGER = logging.getLogger(__name__)

CONF_LOCKS = 'locks'
CONF_USERS = 'users'

CONFIG_SCHEMA = vol.Schema(
    {
        DOMAIN: vol.Schema(
            {
                vol.Optional(CONF_USERS): vol.All(
                    cv.ensure_list,
                    [lambda value: cv.string(value)]
                ),
                vol.Optional(CONF_LOCKS): cv.entity_ids,
            }
        )
    },
    extra=vol.ALLOW_EXTRA,
)


async def async_setup(hass, config):
    users = config[DOMAIN][CONF_USERS] if CONF_USERS in config[DOMAIN] else []
    locks = config[DOMAIN][CONF_LOCKS] if CONF_LOCKS in config[DOMAIN] else []

    component = EntityComponent(_LOGGER, DOMAIN, hass)

    entities = []

    for u in users:
        oid = u.lower().replace(' ', '_')
        user = LockUser(oid, u)
        entities.append(user)

    for l in locks:
        lock = LockManager(hass, l)
        entities.append(lock)

    await component.async_add_entities(entities)

    return True

