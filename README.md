## Example Config

```yaml
lock_manager:
  users:
    - Alice
    - Bob
    - Malory
  locks:
    - front_door
    - back_door
    - office_door
```